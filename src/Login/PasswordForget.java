package Login;

import java.awt.Color;
import java.awt.Container;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.Properties;

import javax.mail.Authenticator;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;

public class PasswordForget extends JFrame implements ActionListener {
	private static final int WIDTH = 400;
	private static final int HEIGHT = 400;
	private JLabel userName;
	private JLabel userMail;
	private JTextField ID = new JTextField();
	private JTextField MAIL = new JTextField();
	private JButton buttonSend = new JButton("發送");
	ReadTXT read = new ReadTXT();

	public PasswordForget() {
		super("PasswordForget");
		setSize(WIDTH, HEIGHT);// 設定視窗大小
		setResizable(false);// 視窗"放大按鈕"無效

		Container container = getContentPane();
		container.setLayout(null);
		container.setBackground(Color.white);

		userName = new JLabel("輸入帳號 : ", JLabel.CENTER);
		userName.setLocation(60, 130);
		userName.setSize(120, 30);
		userName.setFont(new Font("Serif", Font.BOLD, 20));
		container.add(userName);
		ID.setUI(new HintTextFieldUI("請輸入當初註冊之帳號"));
		ID.setLocation(180, 130);
		ID.setSize(135, 30);
		ID.setToolTipText("不可包含下列任意字元: \\ / * ? \" < > | ");

		ID.addKeyListener(new KeyListener() {

			@Override
			public void keyPressed(KeyEvent e) {
				// TODO Auto-generated method stub
				if (e.getKeyCode() == KeyEvent.VK_ENTER) {
					buttonSend.doClick();
				}
			}

			@Override
			public void keyReleased(KeyEvent e) {
				// TODO Auto-generated method stub

			}

			@Override
			public void keyTyped(KeyEvent e) {
				// TODO Auto-generated method stub

			}
		});

		container.add(ID);

		userMail = new JLabel("輸入信箱 : ", JLabel.CENTER);
		userMail.setLocation(60, 170);
		userMail.setSize(120, 30);
		userMail.setFont(new Font("Serif", Font.BOLD, 20));
		container.add(userMail);
		MAIL.setUI(new HintTextFieldUI("請輸入註冊此帳號之信箱"));
		MAIL.setLocation(180, 170);
		MAIL.setSize(135, 30);
		MAIL.setToolTipText("格式如: XXXX@gmail.com");
		MAIL.addKeyListener(new KeyListener() {

			@Override
			public void keyPressed(KeyEvent e) {
				// TODO Auto-generated method stub
				if (e.getKeyCode() == KeyEvent.VK_ENTER) {
					buttonSend.doClick();
				}
			}

			@Override
			public void keyReleased(KeyEvent e) {
				// TODO Auto-generated method stub

			}

			@Override
			public void keyTyped(KeyEvent e) {
				// TODO Auto-generated method stub

			}
		});

		container.add(MAIL);

		buttonSend.setLocation(140, 225);
		buttonSend.setSize(100, 20);
		buttonSend.setFont(new Font("新細明體", Font.BOLD, 14));
		buttonSend.addActionListener(this);
		buttonSend.addKeyListener(new KeyListener() {

			@Override
			public void keyPressed(KeyEvent e) {
				// TODO Auto-generated method stub
				if (e.getKeyCode() == KeyEvent.VK_ENTER) {
					buttonSend.doClick();
				}
			}

			@Override
			public void keyReleased(KeyEvent e) {
				// TODO Auto-generated method stub

			}

			@Override
			public void keyTyped(KeyEvent e) {
				// TODO Auto-generated method stub

			}
		});

		container.add(buttonSend);
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		// TODO Auto-generated method stub

		if (e.getSource() == buttonSend) {
			int sendFlag = 0;
			String id = ID.getText();
			String mail = MAIL.getText();
			String[] ids = read.getID();
			String[] mails = read.getMail();
			String[] passwords = read.getPassword();

			for (int i = 0; i < ids.length; i++) {
				if (id.equals(ids[i]) && mail.equals(mails[i])) {
					JOptionPane.showMessageDialog(this, "信件發送中...");
					String host = "smtp.gmail.com";
					int port = 587;
					final String username = "D0341519@itd.fcu.edu.tw";
					final String password = "m70i38k21e4?";// your password

					Properties props = new Properties();
					props.put("mail.smtp.host", host);
					props.put("mail.smtp.auth", "true");
					props.put("mail.smtp.starttls.enable", "true");
					props.put("mail.smtp.port", port);
					Session session = Session.getInstance(props, new Authenticator() {
						protected PasswordAuthentication getPasswordAuthentication() {
							return new PasswordAuthentication(username, password);
						}
					});

					try {
						Message message = new MimeMessage(session);
						message.setFrom(new InternetAddress("D0341519@itd.fcu.edu.tw"));
						message.setRecipients(Message.RecipientType.TO, InternetAddress.parse(mail));
						message.setSubject("忘記密碼");
						message.setText("Dear User \"" + id + "\" ,請牢記密碼! \"" + passwords[i] + "\"");

						Transport transport = session.getTransport("smtp");
						transport.connect(host, port, username, password);

						Transport.send(message);

						sendFlag = 1;
						JOptionPane.showMessageDialog(this, "密碼已寄至信箱!");
						this.setVisible(false);
					} catch (MessagingException ME) {
						throw new RuntimeException(ME);
					}
				}
			}

			if (sendFlag == 0) {
				JOptionPane.showMessageDialog(this, "請確認帳號及信箱是否正確!");
			}
		}
	}
}