package Login;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Container;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JTextField;

public class LoginTest extends JFrame implements ActionListener {
	// 設定視窗長寬
	private static final int WIDTH = 400;
	private static final int HEIGHT = 400;
	private JLabel userName;
	private JLabel password;
	private JTextField ID = new JTextField();
	private JPasswordField PW = new JPasswordField(8);
	private JButton buttonLogin = new JButton("登入");
	private JButton buttonRegistered = new JButton("註冊");
	private JButton buttonForget = new JButton("忘記密碼");
	static String id = null;
	String pa = null;

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		LoginTest gui = new LoginTest();
		gui.setVisible(true);
	}

	public LoginTest() {
		super("LoginTest");// 呼叫JFrame的建構子, 並設定視窗標題
		setSize(WIDTH, HEIGHT);// 設定視窗大小
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);// 使用者在此程序選擇關閉時預設要執行的操作，使用
														// System exit 方法退出應用程式。
		setResizable(false);// 視窗"放大按鈕"無效

		Container container = getContentPane();
		container.setLayout(null);
		container.setBackground(Color.white);

		// 設定標籤名稱
		userName = new JLabel("User Name : ", JLabel.CENTER);
		userName.setLocation(60, 130);
		userName.setSize(120, 30);
		userName.setFont(new Font("Serif", Font.BOLD, 20));
		container.add(userName);
		ID.setUI(new HintTextFieldUI("請輸入帳號"));
		ID.setLocation(180, 130);
		ID.setSize(120, 30);
		ID.setToolTipText("不可包含下列任意字元: \\ / * ? \" < > |");
		ID.addKeyListener(new KeyListener() {

			@Override
			public void keyPressed(KeyEvent e) {
				// TODO Auto-generated method stub
				if (e.getKeyCode() == KeyEvent.VK_ENTER) {
					buttonLogin.doClick();
				}
			}

			@Override
			public void keyReleased(KeyEvent e) {
				// TODO Auto-generated method stub

			}

			@Override
			public void keyTyped(KeyEvent e) {
				// TODO Auto-generated method stub

			}
		});

		container.add(ID);

		password = new JLabel("Password : ", JLabel.CENTER);
		password.setLocation(60, 170);
		password.setSize(120, 30);
		password.setFont(new Font("Serif", Font.BOLD, 20));
		container.add(password);
		// PW.setUI(new HintTextFieldUI("請輸入密碼"));
		PW.setLocation(180, 170);
		PW.setSize(120, 30);
		PW.setEchoChar('*');
		PW.setToolTipText("密碼長度為8到16個字元之間");
		PW.addKeyListener(new KeyListener() {

			@Override
			public void keyPressed(KeyEvent e) {
				// TODO Auto-generated method stub
				if (e.getKeyCode() == KeyEvent.VK_ENTER) {
					buttonLogin.doClick();
				}
			}

			@Override
			public void keyReleased(KeyEvent e) {
				// TODO Auto-generated method stub

			}

			@Override
			public void keyTyped(KeyEvent e) {
				// TODO Auto-generated method stub

			}
		});

		container.add(PW);

		buttonLogin.setLocation(140, 225);
		buttonLogin.setSize(100, 20);
		buttonLogin.setFont(new Font("新細明體", Font.BOLD, 14));
		buttonLogin.addActionListener(this);
		buttonLogin.addKeyListener(new KeyListener() {

			@Override
			public void keyPressed(KeyEvent e) {
				// TODO Auto-generated method stub
				if (e.getKeyCode() == KeyEvent.VK_ENTER) {
					buttonLogin.doClick();
				}
			}

			@Override
			public void keyReleased(KeyEvent e) {
				// TODO Auto-generated method stub

			}

			@Override
			public void keyTyped(KeyEvent e) {
				// TODO Auto-generated method stub

			}
		});

		container.add(buttonLogin);

		buttonRegistered.setLocation(140, 265);
		buttonRegistered.setSize(100, 20);
		buttonRegistered.setFont(new Font("新細明體", Font.BOLD, 14));
		buttonRegistered.addActionListener(this);
		buttonRegistered.addKeyListener(new KeyListener() {

			@Override
			public void keyPressed(KeyEvent e) {
				// TODO Auto-generated method stub
				if (e.getKeyCode() == KeyEvent.VK_ENTER) {
					buttonRegistered.doClick();
				}
			}

			@Override
			public void keyReleased(KeyEvent e) {
				// TODO Auto-generated method stub

			}

			@Override
			public void keyTyped(KeyEvent e) {
				// TODO Auto-generated method stub

			}
		});

		container.add(buttonRegistered);

		buttonForget.setLocation(140, 305);
		buttonForget.setSize(100, 20);
		buttonForget.setFont(new Font("新細明體", Font.BOLD, 14));
		buttonForget.addActionListener(this);
		buttonForget.addKeyListener(new KeyListener() {

			@Override
			public void keyPressed(KeyEvent e) {
				// TODO Auto-generated method stub
				if (e.getKeyCode() == KeyEvent.VK_ENTER) {
					buttonForget.doClick();
				}
			}

			@Override
			public void keyReleased(KeyEvent e) {
				// TODO Auto-generated method stub

			}

			@Override
			public void keyTyped(KeyEvent e) {
				// TODO Auto-generated method stub

			}
		});

		container.add(buttonForget);
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		// TODO Auto-generated method stub
		id = ID.getText();
		pa = new String(PW.getPassword());
		int loginFlag = 0;

		if (e.getSource() == buttonLogin) {
			ReadTXT read = new ReadTXT();
			String[] ids = read.getID();
			String[] passwords = read.getPassword();

			if (id.trim().isEmpty() || pa.trim().isEmpty()) {
				JOptionPane.showMessageDialog(this, "欄位不可為空!");
			} else {
				if (read.getFileReader() == null) {
					JOptionPane.showMessageDialog(this, "讀檔失敗!");
				} else {
					for (int i = 0; i < ids.length; i++) {
						if (ids[i].equals(id) && passwords[i].equals(pa)) {
							loginFlag = 1;
							Logout gui = new Logout();
							gui.setVisible(true);
							this.setVisible(false);
						}
					}

					if (loginFlag == 0) {
						JOptionPane.showMessageDialog(this, "登入失敗 請確認帳號密碼是否正確!");
					}
				}
			}
		}

		if (e.getSource() == buttonRegistered) {
			Registered gui = new Registered();
			gui.setVisible(true);
		}

		if (e.getSource() == buttonForget) {
			PasswordForget gui = new PasswordForget();
			gui.setVisible(true);
		}
	}

	public static String getID() {
		return id;
	}
}