package Login;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

public class ReadTXT {
	private static int userNumber;
	private static String tempstring;
	private static String[] ids = null;
	private static String[] passwords = null;
	private static String[] mails = null;
	private static String[] tempArray = new String[3]; // ���� id password mail
	FileReader fr = null;
	BufferedReader br = null;

	public ReadTXT() {
		try {
			fr = new FileReader("user.txt");
			br = new BufferedReader(fr);
		} catch (IOException e) {
			System.out.println("Cannot open the file!");
		}

		if (fr != null) {
			try {
				int i = 0;
				String userNum = br.readLine();// Ū�Ĥ@��

				String line = null;
				userNumber = Integer.parseInt(userNum);
				System.out.println("User:" + userNumber);

				ids = new String[userNumber];
				passwords = new String[userNumber];
				mails = new String[userNumber];

				while ((line = br.readLine()) != null) {
					tempstring = line;
					tempArray = tempstring.split("\\s");
					ids[i] = tempArray[0];
					passwords[i] = tempArray[1];
					mails[i] = tempArray[2];
					System.out.println((i + 1) + "_" + ids[i] + "_" + passwords[i] + "_" + mails[i]);
					i++;
				}

				fr.close();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}

	public int getUserNumber() {
		return userNumber;
	}

	public String[] getID() {
		return ids;
	}

	public String[] getPassword() {
		return passwords;
	}

	public String[] getMail() {
		return mails;
	}

	public FileReader getFileReader() {
		return fr;
	}
}
