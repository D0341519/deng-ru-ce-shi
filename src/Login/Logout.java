package Login;

import java.awt.Color;
import java.awt.Container;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;

public class Logout extends JFrame implements ActionListener {
	private static final int WIDTH = 400;
	private static final int HEIGHT = 400;
	private JLabel success;
	private JButton buttonLogout = new JButton("登出");

	public Logout() {
		super("Login success");

		// 設定視窗大小
		setSize(WIDTH, HEIGHT);
		// setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setResizable(false);// 視窗"放大按鈕"無效

		Container container = getContentPane();
		container.setLayout(null);
		container.setBackground(Color.white);

		// 設定標籤名稱
		success = new JLabel(LoginTest.getID() + " 登入成功", JLabel.CENTER);
		success.setLocation(100, 100);
		success.setSize(200, 30);
		success.setFont(new Font("Serif", Font.BOLD, 20));
		container.add(success);

		buttonLogout.setLocation(140, 235);
		buttonLogout.setSize(100, 20);
		buttonLogout.setFont(new Font("新細明體", Font.BOLD, 14));
		buttonLogout.addActionListener(this);
		buttonLogout.addKeyListener(new KeyListener() {

			@Override
			public void keyPressed(KeyEvent e) {
				// TODO Auto-generated method stub
				if (e.getKeyCode() == KeyEvent.VK_ENTER) {
					buttonLogout.doClick();
				}
			}

			@Override
			public void keyReleased(KeyEvent e) {
				// TODO Auto-generated method stub

			}

			@Override
			public void keyTyped(KeyEvent e) {
				// TODO Auto-generated method stub

			}
		});

		container.add(buttonLogout);
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		// TODO Auto-generated method stub
		if (e.getSource() == buttonLogout) {
			int opt = JOptionPane.showConfirmDialog(this, "確定要登出?", "Logout", JOptionPane.YES_NO_OPTION);

			if (opt == JOptionPane.YES_OPTION) {
				this.setVisible(false);
				LoginTest gui = new LoginTest();
				gui.setVisible(true);
			}

		}
	}
}