package Login;

import java.awt.Color;
import java.awt.Container;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.io.FileWriter;
import java.io.IOException;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPasswordField;
import javax.swing.JTextField;

public class Registered extends JFrame implements ActionListener {
	private static final int WIDTH = 400;
	private static final int HEIGHT = 400;
	private JLabel userName;
	private JLabel password;
	private JLabel userMail;
	private JTextField ID = new JTextField(10);
	private JTextField MAIL = new JTextField();
	private JPasswordField PW = new JPasswordField(16);
	private JPasswordField PW2 = new JPasswordField(16);
	private JButton buttonConfirm = new JButton("確認");
	private JButton buttonVerification = new JButton("驗證帳號");
	int verificationFlag;
	String id = null;
	String mail = null;
	String pa = null;
	String pa2 = null;
	String verificationID = null;
	char[] cID = null;
	String[] ids = null;
	String[] passwords = null;
	String[] mails = null;
	ReadTXT read = new ReadTXT();

	public Registered() {
		super("Registered");
		setSize(WIDTH, HEIGHT); // 設定視窗大小
		setResizable(false);// 視窗"放大按鈕"無效

		Container container = getContentPane();
		container.setLayout(null);
		container.setBackground(Color.white);

		// 設定標籤名稱
		userName = new JLabel("輸入帳號 : ", JLabel.CENTER);
		userName.setLocation(35, 90);
		userName.setSize(120, 30);
		userName.setFont(new Font("Serif", Font.BOLD, 20));
		container.add(userName);
		ID.setUI(new HintTextFieldUI("長度限制10字元"));
		ID.setLocation(155, 90);
		ID.setSize(130, 30);
		ID.setToolTipText("不可包含下列任意字元: \\ / * ? \" < > | ");

		ID.addKeyListener(new KeyListener() {

			@Override
			public void keyPressed(KeyEvent e) {
				// TODO Auto-generated method stub
				if (e.getKeyCode() == KeyEvent.VK_ENTER) {
					buttonVerification.doClick();
				}
			}

			@Override
			public void keyReleased(KeyEvent e) {
				// TODO Auto-generated method stub

			}

			@Override
			public void keyTyped(KeyEvent e) {
				// TODO Auto-generated method stub

			}
		});

		container.add(ID);

		password = new JLabel("輸入密碼 : ", JLabel.CENTER);
		password.setLocation(35, 130);
		password.setSize(120, 30);
		password.setFont(new Font("Serif", Font.BOLD, 20));
		container.add(password);
		// PW.setUI(new HintTextFieldUI("請輸入密碼"));
		PW.setLocation(155, 130);
		PW.setSize(130, 30);
		PW.setEchoChar('*');
		PW.setToolTipText("密碼長度請在8到16個字元之間");
		PW.addKeyListener(new KeyListener() {

			@Override
			public void keyPressed(KeyEvent e) {
				// TODO Auto-generated method stub
				if (e.getKeyCode() == KeyEvent.VK_ENTER) {
					buttonConfirm.doClick();
				}
			}

			@Override
			public void keyReleased(KeyEvent e) {
				// TODO Auto-generated method stub

			}

			@Override
			public void keyTyped(KeyEvent e) {
				// TODO Auto-generated method stub

			}
		});

		container.add(PW);

		password = new JLabel("確認密碼 : ", JLabel.CENTER);
		password.setLocation(35, 170);
		password.setSize(120, 30);
		password.setFont(new Font("Serif", Font.BOLD, 20));
		container.add(password);
		// PW2.setUI(new HintTextFieldUI("輸入與上列相同之密碼"));
		PW2.setLocation(155, 170);
		PW2.setSize(130, 30);
		PW2.setEchoChar('*');
		PW2.setToolTipText("請輸入與上列相同之密碼");
		PW2.addKeyListener(new KeyListener() {

			@Override
			public void keyPressed(KeyEvent e) {
				// TODO Auto-generated method stub
				if (e.getKeyCode() == KeyEvent.VK_ENTER) {
					buttonConfirm.doClick();
				}
			}

			@Override
			public void keyReleased(KeyEvent e) {
				// TODO Auto-generated method stub

			}

			@Override
			public void keyTyped(KeyEvent e) {
				// TODO Auto-generated method stub

			}
		});

		container.add(PW2);

		userMail = new JLabel("輸入信箱 : ", JLabel.CENTER);
		userMail.setLocation(35, 210);
		userMail.setSize(120, 30);
		userMail.setFont(new Font("Serif", Font.BOLD, 20));
		container.add(userMail);
		MAIL.setLocation(155, 210);
		MAIL.setSize(130, 30);
		MAIL.setToolTipText("格式如: XXXX@gmail.com");
		MAIL.addKeyListener(new KeyListener() {

			@Override
			public void keyPressed(KeyEvent e) {
				// TODO Auto-generated method stub
				if (e.getKeyCode() == KeyEvent.VK_ENTER) {
					buttonConfirm.doClick();
				}
			}

			@Override
			public void keyReleased(KeyEvent e) {
				// TODO Auto-generated method stub

			}

			@Override
			public void keyTyped(KeyEvent e) {
				// TODO Auto-generated method stub

			}
		});

		container.add(MAIL);

		buttonConfirm.setLocation(140, 265);
		buttonConfirm.setSize(100, 20);
		buttonConfirm.setFont(new Font("新細明體", Font.BOLD, 14));
		buttonConfirm.addActionListener(this);
		buttonConfirm.addKeyListener(new KeyListener() {

			@Override
			public void keyPressed(KeyEvent e) {
				// TODO Auto-generated method stub
				if (e.getKeyCode() == KeyEvent.VK_ENTER) {
					buttonConfirm.doClick();
				}
			}

			@Override
			public void keyReleased(KeyEvent e) {
				// TODO Auto-generated method stub

			}

			@Override
			public void keyTyped(KeyEvent e) {
				// TODO Auto-generated method stub

			}
		});

		container.add(buttonConfirm);

		buttonVerification.setLocation(290, 93);
		buttonVerification.setSize(100, 20);
		buttonVerification.setFont(new Font("新細明體", Font.BOLD, 14));
		buttonVerification.addActionListener(this);
		buttonVerification.addKeyListener(new KeyListener() {

			@Override
			public void keyPressed(KeyEvent e) {
				// TODO Auto-generated method stub
				if (e.getKeyCode() == KeyEvent.VK_ENTER) {
					buttonVerification.doClick();
				}
			}

			@Override
			public void keyReleased(KeyEvent e) {
				// TODO Auto-generated method stub

			}

			@Override
			public void keyTyped(KeyEvent e) {
				// TODO Auto-generated method stub

			}
		});

		container.add(buttonVerification);
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		// TODO Auto-generated method stub
		id = ID.getText();
		mail = MAIL.getText();
		pa = new String(PW.getPassword());
		pa2 = new String(PW2.getPassword());
		cID = id.toCharArray();
		ids = read.getID();
		mails = read.getMail();
		passwords = read.getPassword();
		int mailFlag = 0;

		if (e.getSource() == buttonVerification) {
			if (id.trim().isEmpty()) {
				JOptionPane.showMessageDialog(this, "請輸入帳號!");
			} else if (id.length() > 10) {
				JOptionPane.showMessageDialog(this, "帳號長度請勿大於10個字元");
			} else {
				for (int i = 0; i < ids.length; i++) {
					if (ids[i].equals(id)) {
						verificationFlag = 0;
						JOptionPane.showMessageDialog(this, "此帳號已存在");
						break;
					} else {
						verificationFlag = 1;
					}
				}

				if (verificationFlag == 1) {
					verificationID = id;
					compareString(cID);
				}
			}
		}

		if (e.getSource() == buttonConfirm) {
			if (mail.trim().isEmpty() || id.trim().isEmpty() || pa.trim().isEmpty() || pa2.trim().isEmpty()) {
				JOptionPane.showMessageDialog(this, "欄位不可為空!");
			} else if (isValidEmail(mail) == false) {
				JOptionPane.showMessageDialog(this, "信箱格式不符合  例如:xxxx@gmail.com");
			} else if (pa.length() < 8) {
				JOptionPane.showMessageDialog(this, "密碼長度請勿少於8個字元");
			} else if (pa.length() > 16) {
				JOptionPane.showMessageDialog(this, "密碼長度請勿超過16個字元");
			} else if (pa.equals(pa2) == false) {
				JOptionPane.showMessageDialog(this, "兩次密碼不相符 請重新輸入!");
			} else if (verificationFlag == 0 || id.equals(verificationID) == false) {
				verificationFlag = 0;
				JOptionPane.showMessageDialog(this, "請先檢查帳號是否重複!");
			} else {
				for (int i = 0; i < mails.length; i++) {
					if (mails[i].equals(mail)) {
						mailFlag = 1;
						JOptionPane.showMessageDialog(this, "此信箱已註冊過!");
					}
				}

				if (mailFlag == 0) {
					int userNumber = read.getUserNumber() + 1;
					String str = Integer.toString(userNumber);
					String[] ids2 = new String[userNumber];
					String[] passwords2 = new String[userNumber];
					String[] mails2 = new String[userNumber];

					for (int i = 0; i < userNumber - 1; i++) {
						ids2[i] = ids[i];
						passwords2[i] = passwords[i];
						mails2[i] = mails[i];
					}

					ids2[userNumber - 1] = id;
					passwords2[userNumber - 1] = pa;
					mails2[userNumber - 1] = mail;

					try {
						FileWriter fw = new FileWriter("user.txt");
						fw.write(str + "\r\n");

						for (int i = 0; i < ids2.length; i++) {
							fw.write(ids2[i] + " " + passwords2[i] + " " + mails2[i] + "\r\n");
						}

						fw.flush();
						fw.close();
					} catch (IOException ef) {
						// TODO Auto-generated catch block
						ef.printStackTrace();
					}

					JOptionPane.showMessageDialog(this, "註冊成功!");
					this.setVisible(false);
				}
			}
		}
	}

	// match mail
	public static boolean isValidEmail(String email) {

		if (email == null) {

			return false;
		}

		String emailPattern = "^([\\w]+)(([-\\.][\\w]+)?)*@((\\[[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\.)|(([\\w-]+\\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\\]?)$";

		// String emailPattern =
		// "^[A-Za-z0-9](([_\\.\\-]?[a-zA-Z0-9]+)*)@([A-Za-z0-9]+)(([\\.\\-]?[a-zA-Z0-9]+)*)\\.([A-Za-z]{2,})$";

		return email.matches(emailPattern);
	}

	// 非法字元比對
	public void compareString(char cID[]) {
		String errorString = "[`~!@#$%^&*()+=|{}':;',\\[\\].<>/?~！@#¥%……&*（）——+|{}【】‘；：”“’。，、？]";// 將特殊字元建立成一個字串
		char[] error = errorString.toCharArray();// 將特殊字元轉成字串陣列
		int count = 0;

		for (int a = 0; a < cID.length; a++) {
			for (int b = 0; b < error.length; b++) {
				// System.out.println("this is error array [" + b +"]:
				// "+error[b]);
				if (error[b] == cID[a])// 比較字元是否相等
				{
					count++;
				}
			}
		}

		if (count > 0) {// 顯示訊息
			JOptionPane.showMessageDialog(this, "帳號包含不合法字元，請重新輸入!");
		} else {
			JOptionPane.showMessageDialog(this, "您可以使用此帳號 " + verificationID);
		}
	}
}
